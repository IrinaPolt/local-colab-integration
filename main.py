import subprocess

filename = 'test_script'

subprocess.run(['p2j', f'{filename}.py']) # конвертация файла с кодом в Jupyter Notebook

file_path = f'{filename}.ipynb'

subprocess.run(['colab-cli', 'push-nb', file_path]) # отправка в Google Drive, файл открывается в Colab
