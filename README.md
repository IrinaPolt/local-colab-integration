# Local-colab integration

## Description

The project represents a try to integrate/automate working on local system and sending the code to Google Colab for executing and receiving the output

The work consists of several steps:

1. main.py converts the script.py into Jupyter Notebook file and pushes the file.ipynb into Google Drive using Colab-CLI;

Since Google blocked the opportunity to access Colab without interface, with SSH/remote desktop [link](https://research.google.com/colaboratory/faq.html) etc., the only way we can run the notebook remotely is accessing it from another notebook 

2. runner.ipynb in the notebook [link](https://colab.research.google.com/drive/1qR_8dRiX_PlMQ8CwuE5TRytg3nzFLAN1?usp=sharing) allows to find the pushed file.ipynb, convert it back to python, execute in the cell and download the output in .txt format;

## Project status

By far the functionality only allows to execute the code inside one Google account. Looking forward to test shared access to notebooks and collect the code output the more consistent way
